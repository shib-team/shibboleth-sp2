#!/usr/bin/make -f

# Enable compiler hardening flags.
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

UTIL_INIT = debian/shibboleth-sp-utils.shibd.init
UTIL_SERVICE = debian/shibboleth-sp-utils.shibd.service

ifneq ($(wildcard /usr/include/systemd/sd-daemon.h),)
    enable-systemd = --enable-systemd
    install-service = mv debian/tmp/etc/shibboleth/shibd-systemd $(UTIL_SERVICE)
else
    remove-service = shibd-systemd
endif

%:
	dh $@ --with apache2

override_dh_auto_configure:
	dh_auto_configure -- \
	    --enable-apache-24 --with-apxs24=/usr/bin/apxs2 \
	    $(enable-systemd) \
	    --with-memcached \
	    --with-fastcgi \
	    --with-gssapi

override_dh_auto_install:
	NOKEYGEN=1 dh_auto_install
	find debian/tmp -name "*.la" -delete

docdir = debian/tmp/usr/share/doc/shibboleth-*
override_dh_install:
	# The necessary documentation files are installed in each binary
	# package by dh_installdocs below.
	cd $(docdir) && \
	    rm CREDITS.txt LICENSE.txt NOTICE.txt README.txt RELEASE.txt \
	       FASTCGI.LICENSE OPENSSL.LICENSE LOG4CPP.LICENSE
	# The tag file is unreproducible (it captures the full build path),
	# but is used as a "stamp" file during the build.
	cd $(docdir) && rm -f api/shibboleth.tag api/html/*.md5 api/html/*.map
	$(install-service)
	cd debian/tmp/etc/shibboleth && \
	    rm *.dist *.config shibd-amazon shibd-osx.plist shibd-redhat shibd-suse \
	       $(remove-service)
	mv debian/tmp/etc/shibboleth/keygen.sh debian/tmp/usr/sbin/shib-keygen
	mv debian/tmp/etc/shibboleth/seckeygen.sh debian/tmp/usr/sbin/shib-seckeygen
	mv debian/tmp/etc/shibboleth/metagen.sh debian/tmp/usr/bin/shib-metagen
	mv debian/tmp/etc/shibboleth/shibd-debian $(UTIL_INIT)
	mv debian/tmp/usr/lib/*/shibboleth/mod_shib_24.so debian/mod_shib.so
	mkdir -p debian/tmp/usr/include/$(DEB_HOST_MULTIARCH)/shibsp
	mv debian/tmp/usr/include/shibsp/paths.h \
	    debian/tmp/usr/include/$(DEB_HOST_MULTIARCH)/shibsp/
	dh_install

UTIL_MANS:=$(patsubst %,debian/%, \
    mdquery.1 resolvertest.1 shib-metagen.1 shib-keygen.8 shib-seckeygen.8 shibd.8)

debian/%: debian/man-pages/%.pod
	pod2man --section '$(subst .,,$(suffix $@))' --center Shibboleth --utf8 \
		--release '$(word 3,$(shell ./config.status --version))' $< $@

override_dh_installman: $(UTIL_MANS)
	dh_installman -p shibboleth-sp-utils $(UTIL_MANS)

override_dh_installdocs:
	dh_installdocs -A doc/NOTICE.txt doc/CREDITS.txt doc/RELEASE.txt doc/README.txt

override_dh_installinit:
	dh_installinit --name=shibd

override_dh_installsystemd:
	dh_installsystemd --name=shibd

override_dh_makeshlibs:
	dh_makeshlibs -Xusr/lib/$(DEB_HOST_MULTIARCH)/shibboleth

override_dh_clean:
	dh_clean
	rm -f debian/mod_shib.so $(UTIL_INIT) $(UTIL_SERVICE) $(UTIL_MANS)
