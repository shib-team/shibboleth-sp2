=head1 NAME

shib-keygen - Generate a key pair for a Shibboleth SP

=head1 SYNOPSIS

B<shib-keygen> [B<-bf>] [B<-e> I<entity-id>] [B<-g> I<group>] [B<-n> I<prefix>]
    [B<-h> I<hostname>] [B<-o> I<output-dir>] [B<-u> I<user>] [B<-y> I<years>]

=head1 DESCRIPTION

Generate a self-signed X.509 certificate for a Shibboleth SP.  By default,
the certificate will be for the local fully-qualified (as returned by
C<hostname --fqdn>) hostname.  An entity ID can be specified with the
B<-e> flag.  The B<openssl> command-line client is used to generate the
key pair.  By default, the public certificate will be created in
F</etc/shibboleth/sp-cert.pem> and the private key in
F</etc/shibboleth/sp-key.pem>.

=head1 OPTIONS

=over 4

=item B<-b>

Batch mode: exit successfully without doing anything if F<sp-key.pem> or
F<sp-cert.pem> already exists, unless B<-f> was also specified.
Suppress standard error output from B<openssl> when creating the certificate.

=item B<-e> I<entity-id>

Add I<entity-id> (which should be a URI) as an alternative name for the
certificate.

=item B<-f>

Remove F<sp-cert.pem> and F<sp-key.pem>
before generating a new certificate.  Without this option, if those files
already exist, B<shib-keygen> prints an error and exits rather than
overwriting them.

=item B<-g> I<group>

After generating the key and certificate, change the group ownership of
the key file to this group.  By default, the group used is C<_shibd>.

=item B<-h> I<hostname>

Specify the fully-qualified domain name for which to generate a
certificate.  If this option isn't given, the hostname defaults to the
result of C<hostname --fqdn>.

=item B<-o> I<output-dir>

Store F<sp-cert.pem> and F<sp-key.pem> in the directory I<output-dir>
rather than the default of F</etc/shibboleth>.

=item B<-n> I<prefix>

Use I<prefix> instead of F<sp> in the name of the generated certificate
and private key file.

=item B<-u> I<user>

After generating the key and certificate, change the ownership of the key
file to this user.  This is used to allow the key to be read by a non-root
user so that B<shibd> can be run as a non-root user.  By default, the
key is owned by C<_shibd>.

=item B<-y> I<years>

The number of years for which the certificate should be valid.  The
default expiration time is ten years into the future.

=back

=head1 FILES

=over 4

=item F</etc/shibboleth/sp-cert.cnf>

The OpenSSL configuration file used for generating the self-signed
certificate.  This configuration file is generated when the script is run
and deleted afterwards.

=item F</etc/shibboelth/sp-cert.pem>

The default location of the public certificate created by this script.

=item F</etc/shibboleth/sp-key.pem>

The default location of the private key for the certificate created by
this script.

=back

These three files are stored in the directory given with B<-o> instead, if
that option is given.

=head1 AUTHOR

This manual page was written by Russ Allbery for Debian GNU/Linux.

=head1 COPYRIGHT

Copyright 2008, 2011 Russ Allbery.  This manual page is hereby placed into
the public domain by its author.

=cut
