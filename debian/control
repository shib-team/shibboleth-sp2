Source: shibboleth-sp
Section: web
Priority: optional
Maintainer: Debian Shib Team <pkg-shibboleth-devel@alioth-lists.debian.net>
Uploaders:
 Ferenc Wágner <wferi@debian.org>,
Build-Depends: dpkg-dev (>= 1.22.5),
 apache2-dev,
 cxxtest,
 debhelper-compat (= 13),
 dh-apache2,
 libboost-dev,
 libfcgi-dev,
 libkrb5-dev,
 liblog4shib-dev,
 libmemcached-dev,
 libsaml-dev (>= 3.3~),
 libsystemd-dev [linux-any],
 libxerces-c-dev,
 libxml-security-c-dev,
 libxmltooling-dev (>= 3.3~),
 opensaml-schemas (>= 3.3~),
 pkgconf,
 unixodbc-dev,
 xmltooling-schemas,
Build-Depends-Indep:
 doxygen,
 graphviz,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://shibboleth.net/
Vcs-Git: https://salsa.debian.org/shib-team/shibboleth-sp2.git
Vcs-Browser: https://salsa.debian.org/shib-team/shibboleth-sp2
X-Common-Description: The Shibboleth System is a standards based software
 package for web single sign-on across or within organizational boundaries.
 It supports authorization and attribute exchange using the OASIS SAML 2.0
 protocol.  Shibboleth allows sites to make informed authorization decisions
 for individual access of protected online resources while allowing users to
 establish their identities with their local authentication systems.

Package: libapache2-mod-shib
Section: httpd
Architecture: any
Depends:
 libshibsp-plugins (= ${binary:Version}),
 shibboleth-sp-utils (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${misc:Recommends},
Description: Federated web single sign-on system (Apache module)
 ${S:X-Common-Description}
 .
 This package contains the Shibboleth Apache module for service providers
 (web servers providing resources protected by Shibboleth) and the
 supporting shibd daemon.

Package: libshibsp12
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 opensaml-schemas (>= 3.3~),
# any->all dependencies mustn't use ${binary:Version}:
 shibboleth-sp-common (= ${source:Version}),
 xmltooling-schemas (>= 3.3~),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Federated web single sign-on system (runtime)
 ${S:X-Common-Description}
 .
 This package contains the Shibboleth SP runtime library.

Package: libshibsp-plugins
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Federated web single sign-on system (plugins)
 ${S:X-Common-Description}
 .
 This package contains plugins for the Shibboleth SP library.

Package: libshibsp-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libsaml-dev (>= 3.3~),
 libshibsp12 (= ${binary:Version}),
 libxerces-c-dev,
 libxmltooling-dev (>= 3.3~),
 ${misc:Depends},
Suggests:
 libshibsp-doc,
Description: Federated web single sign-on system (development)
 ${S:X-Common-Description}
 .
 This package contains the headers and other necessary files to build
 applications that use the Shibboleth SP library.

Package: libshibsp-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: Federated web single sign-on system (API docs)
 ${S:X-Common-Description}
 .
 This package contains the Shibboleth SP library API documentation.

Package: shibboleth-sp-common
Section: libs
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: Federated web single sign-on system (common files)
 ${S:X-Common-Description}
 .
 This package contains common files used by the Shibboleth SP library,
 Apache module, and daemon, primarily configuration files and schemas.

Package: shibboleth-sp-utils
Architecture: any
Multi-Arch: foreign
# The fix for #887904 requires recent init-system-helpers:
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 adduser,
 libshibsp-plugins (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 openssl,
Description: Federated web single sign-on system (daemon and utilities)
 ${S:X-Common-Description}
 .
 This package contains the daemon that handles attribute requests and
 maintains session information for the SP.  It is used internally by the
 Apache module (libapache2-mod-shib), but may be useful independently in
 some circumstances.  It also contains some other useful Shibboleth SP
 utility programs and the FastCGI authorizer and responder.
